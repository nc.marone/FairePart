(function () {
    // Add event listener
    const elem = document.querySelector("#parallax");

    const isVertical = window.orientation > 1;
    if (isVertical) {
        animateMobile();
    } else {
        document.addEventListener("mousemove", parallax);
    }

    function animateMobile() { }

    // Magic happens here
    function parallax(e) {
        let _w = window.innerWidth / 2;
        let _h = window.innerHeight / 2;
        let _mouseX = e.clientX;
        let _mouseY = e.clientY;
        let _depth1 = `${50 - (_mouseX - _w) * 0.01}% ${50 - (_mouseY - _h) * 0.01}%`;
        let _depth2 = `${50 - (_mouseX - _w) * 0.01}% ${8 - (_mouseY - _h) * 0.01}%`;
        let _depth3 = `${50 - (_mouseX - _w) * 0.03}% ${80 - (_mouseY - _h) * 0.03}%`;
        let x = `${_depth3}, ${_depth2}, ${_depth1}`;
        console.log(x);
        elem.style.backgroundPosition = x;
    }


    window.addeventasync = function () {
        addeventatc.settings({
            //css: false
        });
    };
})();